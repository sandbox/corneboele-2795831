<?php

namespace Drupal\i18n_access;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Modifies the content translation access check service.
 */
class I18nAccessServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {

    if($container->hasDefinition('content_translation.manage_access')) {
      $definition = $container->getDefinition('content_translation.manage_access');
      $definition->setClass('Drupal\i18n_access\Access\ContentTranslationManageAccessCheck');
    }
  }
}